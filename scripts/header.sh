set -o errexit -o nounset -o noglob -o pipefail
command -v shopt > /dev/null && shopt -s failglob
(
    . /etc/os-release
    echo \
        $PRETTY_NAME \
        ${VERSION_ID:+$VERSION_ID}
)
export TERM="xterm-color"
# export TIME="$(printf '\ne:\e[90;47m')%e$(printf '\e[0m') U:%U S:%S seconds I:%I O:%O M:%M"
export TIME="$(printf '\n\e[90;47m')%e$(printf '\e[0m')"
export TIMEFORMAT="$(printf '\n\e[90;47m')%R$(printf '\e[0m seconds')"
    #^ for bash time

export BAT_STYLE="changes,header-filename,rule,snip"
export BAT_THEME="ansi"
BAT () { $BAT \
    --terminal-width $COLUMNS \
    --color always \
    --decorations always \
    --italic-text always \
  $@ ; }


